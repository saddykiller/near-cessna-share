
near create-account cessna.saddykiller.testnet --masterAccount saddykiller.testnet --initialBalance 10    

near deploy --wasmFile contract/res/main.wasm --accountId cessna.saddykiller.testnet      

near dev-deploy --wasmFile contract/res/main.wasm

export ID=dev-1638364200676-75708559338339

export ID=cessna.saddykiller.testnet

near view $ID show_time_slots '{}'

near call $ID  add_time_slot '{"option": "10 Dec 2021"}' --accountId $ID 
near call $ID  add_time_slot '{"option": "11 Dec 2021"}' --accountId $ID 
near call $ID  add_time_slot '{"option": "12 Dec 2021"}' --accountId $ID 
near call $ID  add_time_slot '{"option": "13 Dec 2021"}' --accountId $ID
near call $ID  add_time_slot '{"option": "14 Dec 2021"}' --accountId $ID
near call $ID  add_time_slot '{"option": "15 Dec 2021"}' --accountId $ID
near call $ID  add_time_slot '{"option": "16 Dec 2021"}' --accountId $ID
near call $ID  add_time_slot '{"option": "17 Dec 2021"}' --accountId $ID

near call vote.saddykiller.testnet  vote '{"candidate_id": 1}' --accountId vote.saddykiller.testnet 

     