#!/bin/bash

set -e
rm -f res/main.wasm
RUSTFLAGS="-C link-args=-s" cargo build --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/saddykiller_vote.wasm res/main.wasm