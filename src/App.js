import "regenerator-runtime/runtime";
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Big from "big.js";
import CessnaPhoto from "./cessna.png";

//const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();
const DONATION = Big("0.000000000000000000000001")
  .times(10 ** 24)
  .toFixed();
const BOATLOAD_OF_GAS = Big(3)
  .times(10 ** 13)
  .toFixed();
const NEAR_FOR_CALL = Big("0.00125")
  .times(10 ** 24)
  .toFixed();

const freeString = inputStr => {
  if (inputStr) {
   console.log(inputStr)
  }
  return false
}

const App = ({ contract, currentUser, nearConfig, wallet }) => {
  const [candidates, setCandidates] = useState([[['1 Dec. 2021'], ['']],[['2 Dec. 2021'], ['TestUser2']],[['3 Dec. 2021'], ['TestUser3']]]);
  const [newCandidate, setNewCandidate] = useState("");
  const [alreadyVote, setAlreadyVote] = useState(false);

  useEffect(() => {
    getAlreadyVote();
    checkVotes();
  }, [newCandidate]);

  const getAlreadyVote = () => {
    if (!currentUser) return;
    return contract
      .get_already_vote({
        account_id: currentUser.accountId,
      })
      .then((voted) => {
        console.log(currentUser.accountId, "Already vote?:", voted);
        setAlreadyVote(voted);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const vote = (time_slot) => {
    console.log("You vote for - ", candidates[time_slot]);
    contract
      .get_time({
        time_slot,
        account_id: currentUser.accountId,
      })
      .then(() => checkVotes())
      .then(() => getAlreadyVote())
      .catch((e) => {
        console.error(e);
      });
  };

  const onAddNewCandidate = (e) => {
    console.log("My candidate:", newCandidate);
    contract
      .add_option({
        option: newCandidate,
        account_id: currentUser.accountId,
      })
      .then(() => {
        setNewCandidate("");
        return checkVotes();
      })
      .then(() => getAlreadyVote())
      .catch((e) => {
        console.error(e);
      });
  };

  const onNewCandidateChange = (e) => {
    setNewCandidate(e.target.value);
  };

  const checkVotes = () => {
    return contract
      .show_time_slots()
      .then((votes) => {
        setCandidates(votes);
        console.log("Votes:", votes);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const signIn = () => {
    wallet
      .requestSignIn(
        nearConfig.contractName,
        "Share Cessna now! It's time to fly!"
      )
      .then((res) =>
        contract.storage_deposit(
          {
            account_id: currentUser.accountId,
          },
          BOATLOAD_OF_GAS,
          Big("0.00125")
            .times(10 ** 24)
            .toFixed()
        )
      )
      .catch((e) => console.log(e));
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <main>
      <header>
        <h1>Share Cessna now! It{"'"}s time to fly!</h1>
        {currentUser ? (
          <button onClick={signOut}>Log out</button>
        ) : (
          <>
            <h3>Login to get your slot!</h3>
            <button onClick={signIn}>Log in</button>
          </>
        )}
      </header>
      {alreadyVote ? <h3>Sory, you already take your slot.</h3> : ""}

      <div className='nftContainer'> 
      <img className='nft' src={CessnaPhoto}></img>
      </div>
      

      <div className='orderTable'>
        <table>
          <thead>
            <tr>
              <th scope="col">Slot</th>
              <th scope="col">User name</th>
              <th scope="col">Take</th>
            </tr>
          </thead>
          <tbody>
            {candidates.map((candidate) => {
              const itFreeTime = freeString(candidate[1]);
              console.log(candidate[1], itFreeTime)
              return (
                <tr className="voteRow" key={candidate[0]}>
                  <td className="candidateCol">{candidate[0]}</td>
                  <td className="candidateVotes">{itFreeTime ? 'Free' : candidate[1]}</td>
                  <td className="candidateVoteButton">
                    <button type='button' disabled={!currentUser && !itFreeTime}
                      className="voteButton"
                      onClick={() => vote(candidate[0])}
                    >
                      Take
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </main>
  );
};

App.propTypes = {
  contract: PropTypes.shape({
    show_time_slots: PropTypes.func.isRequired,
  }).isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired,
  }),
  nearConfig: PropTypes.shape({
    contractName: PropTypes.string.isRequired,
  }).isRequired,
  wallet: PropTypes.shape({
    requestSignIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
  }).isRequired,
};

export default App;
